var app = require('express')();
const chrome = require('chrome-aws-lambda');
const puppeteer = require('puppeteer-core');
const axios = require("axios");
const $ = require("cheerio");
const fs = require("fs");

app.get("/*", async function(req,res) {
try {
    const browser = await puppeteer.launch({
        args: chrome.args,
        executablePath: await chrome.executablePath,
        headless: chrome.headless
    });
    const page = await browser.newPage();
    page.on('dialog', async dialog => {
    await dialog.accept();
  });
    const userAgent = 'Mozilla/5.0 (X11; Linux x86_64)' +
  'AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.39 Safari/537.36';
    await page.setUserAgent(userAgent);
    await page.setViewport({
      'width': 375,
      'height': 812,
      'deviceScaleFactor': 3,
      'isMobile': true,
      'hasTouch': true,
      'isLandscape': false
    });
    await page.evaluateOnNewDocument(() => {
  Object.defineProperty(navigator, 'webdriver', {
    get: () => false,
  });
});
    await page.evaluateOnNewDocument(() => {
  // We can mock this in as much depth as we need for the test.
  window.navigator.chrome = {
    runtime: {},
    // etc.
  };
});
    await page.evaluateOnNewDocument(() => {
  const originalQuery = window.navigator.permissions.query;
  return window.navigator.permissions.query = (parameters) => (
    parameters.name === 'notifications' ?
      Promise.resolve({ state: Notification.permission }) :
      originalQuery(parameters)
  );
});
    await page.evaluateOnNewDocument(() => {
  // Overwrite the `plugins` property to use a custom getter.
  Object.defineProperty(navigator, 'plugins', {
    // This just needs to have `length > 0` for the current test,
    // but we could mock the plugins too if necessary.
    get: () => [1, 2, 3, 4, 5],
  });
});
    await page.evaluateOnNewDocument(() => {
  // Overwrite the `plugins` property to use a custom getter.
  Object.defineProperty(navigator, 'languages', {
    get: () => ['en-US', 'en'],
  });
});
    await page.goto("https://twitter.com/login");
    await page.waitForSelector("#page-container > div > div.signin-wrapper > form > div.clearfix > button", { timeout: 5000 });
    await page.$eval('#page-container > div > div.signin-wrapper > form > fieldset > div:nth-child(2) > input', el => el.value = 'qwertyxx99xx99@gmail.com');
    await page.$eval('#page-container > div > div.signin-wrapper > form > fieldset > div:nth-child(3) > input', el => el.value = 'Passsw0rd');
    await page.click("#page-container > div > div.signin-wrapper > form > div.clearfix > button");
    await page.waitFor(750);
    res.setHeader("Access-Control-Allow-Origin", "*");
    res.type("image/png").end(await page.screenshot());
    await browser.close();
}
catch(err) {
    res.setHeader("Access-Control-Allow-Origin", "*");
    res.send(err);
   }
})

app.listen(process.env.PORT);
